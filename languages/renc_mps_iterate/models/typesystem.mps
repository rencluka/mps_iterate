<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:6d0ddc3e-cdee-44ad-b512-eca1c29c4e3e(renc_mps_iterate.typesystem)">
  <persistence version="9" />
  <languages>
    <use id="7a5dda62-9140-4668-ab76-d5ed1746f2b2" name="jetbrains.mps.lang.typesystem" version="0" />
    <devkit ref="2677cb18-f558-4e33-bc38-a5139cee06dc(jetbrains.mps.devkit.language-design)" />
  </languages>
  <imports>
    <import index="zz55" ref="r:118cddf0-50eb-4508-b43c-042dc187977c(renc_mps_iterate.structure)" implicit="true" />
    <import index="tpee" ref="r:00000000-0000-4000-0000-011c895902ca(jetbrains.mps.baseLanguage.structure)" implicit="true" />
  </imports>
  <registry>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1145552977093" name="jetbrains.mps.baseLanguage.structure.GenericNewExpression" flags="nn" index="2ShNRf">
        <child id="1145553007750" name="creator" index="2ShVmc" />
      </concept>
      <concept id="1068580123157" name="jetbrains.mps.baseLanguage.structure.Statement" flags="nn" index="3clFbH" />
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
    </language>
    <language id="7a5dda62-9140-4668-ab76-d5ed1746f2b2" name="jetbrains.mps.lang.typesystem">
      <concept id="1185788614172" name="jetbrains.mps.lang.typesystem.structure.NormalTypeClause" flags="ng" index="mw_s8">
        <child id="1185788644032" name="normalType" index="mwGJk" />
      </concept>
      <concept id="1195213580585" name="jetbrains.mps.lang.typesystem.structure.AbstractCheckingRule" flags="ig" index="18hYwZ">
        <child id="1195213635060" name="body" index="18ibNy" />
      </concept>
      <concept id="1174642788531" name="jetbrains.mps.lang.typesystem.structure.ConceptReference" flags="ig" index="1YaCAy">
        <reference id="1174642800329" name="concept" index="1YaFvo" />
      </concept>
      <concept id="1174643105530" name="jetbrains.mps.lang.typesystem.structure.InferenceRule" flags="ig" index="1YbPZF" />
      <concept id="1174648085619" name="jetbrains.mps.lang.typesystem.structure.AbstractRule" flags="ng" index="1YuPPy">
        <child id="1174648101952" name="applicableNode" index="1YuTPh" />
      </concept>
      <concept id="1174650418652" name="jetbrains.mps.lang.typesystem.structure.ApplicableNodeReference" flags="nn" index="1YBJjd">
        <reference id="1174650432090" name="applicableNode" index="1YBMHb" />
      </concept>
      <concept id="1174657487114" name="jetbrains.mps.lang.typesystem.structure.TypeOfExpression" flags="nn" index="1Z2H0r">
        <child id="1174657509053" name="term" index="1Z2MuG" />
      </concept>
      <concept id="1174658326157" name="jetbrains.mps.lang.typesystem.structure.CreateEquationStatement" flags="nn" index="1Z5TYs" />
      <concept id="1174660718586" name="jetbrains.mps.lang.typesystem.structure.AbstractEquationStatement" flags="nn" index="1Zf1VF">
        <child id="1174660783413" name="leftExpression" index="1ZfhK$" />
        <child id="1174660783414" name="rightExpression" index="1ZfhKB" />
      </concept>
      <concept id="1174663118805" name="jetbrains.mps.lang.typesystem.structure.CreateLessThanInequationStatement" flags="nn" index="1ZobV4" />
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="1180636770613" name="jetbrains.mps.lang.smodel.structure.SNodeCreator" flags="nn" index="3zrR0B">
        <child id="1180636770616" name="createdType" index="3zrR0E" />
      </concept>
      <concept id="1138055754698" name="jetbrains.mps.lang.smodel.structure.SNodeType" flags="in" index="3Tqbb2">
        <reference id="1138405853777" name="concept" index="ehGHo" />
      </concept>
      <concept id="1138056143562" name="jetbrains.mps.lang.smodel.structure.SLinkAccess" flags="nn" index="3TrEf2">
        <reference id="1138056516764" name="link" index="3Tt5mk" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="1YbPZF" id="5LkYH4DA24j">
    <property role="TrG5h" value="typeof_high" />
    <node concept="3clFbS" id="5LkYH4DA24k" role="18ibNy">
      <node concept="1Z5TYs" id="5LkYH4DAg3D" role="3cqZAp">
        <node concept="mw_s8" id="5LkYH4DAg3V" role="1ZfhKB">
          <node concept="2OqwBi" id="5LkYH4DAgb8" role="mwGJk">
            <node concept="1YBJjd" id="5LkYH4DAg3T" role="2Oq$k0">
              <ref role="1YBMHb" node="5LkYH4DA2nF" resolve="high" />
            </node>
            <node concept="3TrEf2" id="5LkYH4DAgtD" role="2OqNvi">
              <ref role="3Tt5mk" to="zz55:5LkYH4D_h0L" resolve="value" />
            </node>
          </node>
        </node>
        <node concept="mw_s8" id="5LkYH4DAg3G" role="1ZfhK$">
          <node concept="1Z2H0r" id="5LkYH4DAfyl" role="mwGJk">
            <node concept="1YBJjd" id="5LkYH4DAf$5" role="1Z2MuG">
              <ref role="1YBMHb" node="5LkYH4DA2nF" resolve="high" />
            </node>
          </node>
        </node>
      </node>
      <node concept="1ZobV4" id="5LkYH4DAhTx" role="3cqZAp">
        <node concept="mw_s8" id="5LkYH4DAhU1" role="1ZfhKB">
          <node concept="2ShNRf" id="5LkYH4DAi4_" role="mwGJk">
            <node concept="3zrR0B" id="5LkYH4DAid5" role="2ShVmc">
              <node concept="3Tqbb2" id="5LkYH4DAid7" role="3zrR0E">
                <ref role="ehGHo" to="tpee:f_0OyhT" resolve="IntegerType" />
              </node>
            </node>
          </node>
        </node>
        <node concept="mw_s8" id="5LkYH4DAhT$" role="1ZfhK$">
          <node concept="1Z2H0r" id="5LkYH4DAgII" role="mwGJk">
            <node concept="2OqwBi" id="5LkYH4DAgRR" role="1Z2MuG">
              <node concept="1YBJjd" id="5LkYH4DAgKC" role="2Oq$k0">
                <ref role="1YBMHb" node="5LkYH4DA2nF" resolve="high" />
              </node>
              <node concept="3TrEf2" id="5LkYH4DAhe2" role="2OqNvi">
                <ref role="3Tt5mk" to="zz55:5LkYH4D_h0L" resolve="value" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="3clFbH" id="5LkYH4DAhVA" role="3cqZAp" />
    </node>
    <node concept="1YaCAy" id="5LkYH4DA2nF" role="1YuTPh">
      <property role="TrG5h" value="high" />
      <ref role="1YaFvo" to="zz55:5LkYH4D_eTS" resolve="high" />
    </node>
  </node>
  <node concept="1YbPZF" id="5LkYH4DAjjA">
    <property role="TrG5h" value="typeof_low" />
    <node concept="3clFbS" id="5LkYH4DAjjB" role="18ibNy">
      <node concept="1Z5TYs" id="5LkYH4DAjx2" role="3cqZAp">
        <node concept="mw_s8" id="5LkYH4DAjxk" role="1ZfhKB">
          <node concept="2OqwBi" id="5LkYH4DAjE7" role="mwGJk">
            <node concept="1YBJjd" id="5LkYH4DAjxi" role="2Oq$k0">
              <ref role="1YBMHb" node="5LkYH4DAjls" resolve="low" />
            </node>
            <node concept="3TrEf2" id="5LkYH4DAjNL" role="2OqNvi">
              <ref role="3Tt5mk" to="zz55:5LkYH4D_h0N" resolve="value" />
            </node>
          </node>
        </node>
        <node concept="mw_s8" id="5LkYH4DAjx5" role="1ZfhK$">
          <node concept="1Z2H0r" id="5LkYH4DAjlE" role="mwGJk">
            <node concept="1YBJjd" id="5LkYH4DAjns" role="1Z2MuG">
              <ref role="1YBMHb" node="5LkYH4DAjls" resolve="low" />
            </node>
          </node>
        </node>
      </node>
      <node concept="1ZobV4" id="5LkYH4DAk1A" role="3cqZAp">
        <node concept="mw_s8" id="5LkYH4DAk24" role="1ZfhKB">
          <node concept="2ShNRf" id="5LkYH4DAk2K" role="mwGJk">
            <node concept="3zrR0B" id="5LkYH4DAkbg" role="2ShVmc">
              <node concept="3Tqbb2" id="5LkYH4DAkbi" role="3zrR0E">
                <ref role="ehGHo" to="tpee:f_0OyhT" resolve="IntegerType" />
              </node>
            </node>
          </node>
        </node>
        <node concept="mw_s8" id="5LkYH4DAk1D" role="1ZfhK$">
          <node concept="1Z2H0r" id="5LkYH4DAjQ6" role="mwGJk">
            <node concept="2OqwBi" id="5LkYH4DAlZ0" role="1Z2MuG">
              <node concept="1YBJjd" id="5LkYH4DAjS0" role="2Oq$k0">
                <ref role="1YBMHb" node="5LkYH4DAjls" resolve="low" />
              </node>
              <node concept="3TrEf2" id="5LkYH4DAmaR" role="2OqNvi">
                <ref role="3Tt5mk" to="zz55:5LkYH4D_h0N" resolve="value" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="1YaCAy" id="5LkYH4DAjls" role="1YuTPh">
      <property role="TrG5h" value="low" />
      <ref role="1YaFvo" to="zz55:5LkYH4D_eV_" resolve="low" />
    </node>
  </node>
</model>

