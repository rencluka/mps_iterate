<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:6f8bead2-5fdf-4231-a436-a08c81ad22d2(renc_mps_iterate.editor)">
  <persistence version="9" />
  <languages>
    <use id="18bc6592-03a6-4e29-a83a-7ff23bde13ba" name="jetbrains.mps.lang.editor" version="7" />
    <devkit ref="2677cb18-f558-4e33-bc38-a5139cee06dc(jetbrains.mps.devkit.language-design)" />
  </languages>
  <imports>
    <import index="zz55" ref="r:118cddf0-50eb-4508-b43c-042dc187977c(renc_mps_iterate.structure)" implicit="true" />
    <import index="tpen" ref="r:00000000-0000-4000-0000-011c895902c3(jetbrains.mps.baseLanguage.editor)" implicit="true" />
    <import index="tpck" ref="r:00000000-0000-4000-0000-011c89590288(jetbrains.mps.lang.core.structure)" implicit="true" />
  </imports>
  <registry>
    <language id="18bc6592-03a6-4e29-a83a-7ff23bde13ba" name="jetbrains.mps.lang.editor">
      <concept id="1071666914219" name="jetbrains.mps.lang.editor.structure.ConceptEditorDeclaration" flags="ig" index="24kQdi" />
      <concept id="1140524381322" name="jetbrains.mps.lang.editor.structure.CellModel_ListWithRole" flags="ng" index="2czfm3">
        <child id="1140524464360" name="cellLayout" index="2czzBx" />
        <child id="1140524464359" name="emptyCellModel" index="2czzBI" />
      </concept>
      <concept id="1237303669825" name="jetbrains.mps.lang.editor.structure.CellLayout_Indent" flags="nn" index="l2Vlx" />
      <concept id="1237308012275" name="jetbrains.mps.lang.editor.structure.IndentLayoutNewLineStyleClassItem" flags="ln" index="ljvvj" />
      <concept id="1080736578640" name="jetbrains.mps.lang.editor.structure.BaseEditorComponent" flags="ig" index="2wURMF">
        <child id="1080736633877" name="cellModel" index="2wV5jI" />
      </concept>
      <concept id="1186414536763" name="jetbrains.mps.lang.editor.structure.BooleanStyleSheetItem" flags="ln" index="VOi$J">
        <property id="1186414551515" name="flag" index="VOm3f" />
      </concept>
      <concept id="1381004262292414836" name="jetbrains.mps.lang.editor.structure.ICellStyle" flags="ng" index="1k5N5V">
        <reference id="1381004262292426837" name="parentStyleClass" index="1k5W1q" />
      </concept>
      <concept id="1139848536355" name="jetbrains.mps.lang.editor.structure.CellModel_WithRole" flags="ng" index="1$h60E">
        <reference id="1140103550593" name="relationDeclaration" index="1NtTu8" />
      </concept>
      <concept id="1073389446423" name="jetbrains.mps.lang.editor.structure.CellModel_Collection" flags="sn" stub="3013115976261988961" index="3EZMnI">
        <child id="1106270802874" name="cellLayout" index="2iSdaV" />
        <child id="1073389446424" name="childCellModel" index="3EZMnx" />
      </concept>
      <concept id="1073389577006" name="jetbrains.mps.lang.editor.structure.CellModel_Constant" flags="sn" stub="3610246225209162225" index="3F0ifn">
        <property id="1073389577007" name="text" index="3F0ifm" />
      </concept>
      <concept id="1073389658414" name="jetbrains.mps.lang.editor.structure.CellModel_Property" flags="sg" stub="730538219796134133" index="3F0A7n" />
      <concept id="1219418625346" name="jetbrains.mps.lang.editor.structure.IStyleContainer" flags="ng" index="3F0Thp">
        <child id="1219418656006" name="styleItem" index="3F10Kt" />
      </concept>
      <concept id="1073389882823" name="jetbrains.mps.lang.editor.structure.CellModel_RefNode" flags="sg" stub="730538219795960754" index="3F1sOY" />
      <concept id="1073390211982" name="jetbrains.mps.lang.editor.structure.CellModel_RefNodeList" flags="sg" stub="2794558372793454595" index="3F2HdR" />
      <concept id="1166049232041" name="jetbrains.mps.lang.editor.structure.AbstractComponent" flags="ng" index="1XWOmA">
        <reference id="1166049300910" name="conceptDeclaration" index="1XX52x" />
      </concept>
    </language>
  </registry>
  <node concept="24kQdi" id="5LkYH4D_ezB">
    <ref role="1XX52x" to="zz55:5LkYH4D_dGf" resolve="Iterate" />
    <node concept="3EZMnI" id="5LkYH4D_ezD" role="2wV5jI">
      <node concept="3F0ifn" id="5LkYH4DA6YQ" role="3EZMnx">
        <property role="3F0ifm" value="iterate" />
        <ref role="1k5W1q" to="tpen:hgVS8CF" resolve="KeyWord" />
      </node>
      <node concept="3F1sOY" id="5LkYH4D_iIj" role="3EZMnx">
        <ref role="1NtTu8" to="zz55:5LkYH4D_eTQ" resolve="var" />
        <ref role="1k5W1q" to="tpen:hshT4rC" resolve="NumericLiteral" />
      </node>
      <node concept="3F0ifn" id="5LkYH4D_eAw" role="3EZMnx">
        <property role="3F0ifm" value="from" />
        <ref role="1k5W1q" to="tpen:hgVS8CF" resolve="KeyWord" />
      </node>
      <node concept="3F1sOY" id="5LkYH4D_eWd" role="3EZMnx">
        <ref role="1NtTu8" to="zz55:5LkYH4D_eVD" resolve="low" />
        <ref role="1k5W1q" to="tpen:hshT4rC" resolve="NumericLiteral" />
      </node>
      <node concept="3F0ifn" id="5LkYH4D_eAQ" role="3EZMnx">
        <property role="3F0ifm" value="to" />
        <ref role="1k5W1q" to="tpen:hgVS8CF" resolve="KeyWord" />
      </node>
      <node concept="3F1sOY" id="5LkYH4D_eWr" role="3EZMnx">
        <ref role="1NtTu8" to="zz55:5LkYH4D_eVG" resolve="high" />
        <ref role="1k5W1q" to="tpen:hshT4rC" resolve="NumericLiteral" />
      </node>
      <node concept="3F0ifn" id="5LkYH4D_eBk" role="3EZMnx">
        <property role="3F0ifm" value="{" />
        <ref role="1k5W1q" to="tpen:hXb$RYA" resolve="LeftBracket" />
        <node concept="ljvvj" id="5LkYH4D_eBt" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F2HdR" id="5LkYH4DA66H" role="3EZMnx">
        <ref role="1NtTu8" to="zz55:5LkYH4D_eVK" resolve="body" />
        <node concept="l2Vlx" id="5LkYH4DA66J" role="2czzBx" />
        <node concept="ljvvj" id="5LkYH4DAoOS" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="3F0ifn" id="5LkYH4DAqMr" role="2czzBI">
          <property role="3F0ifm" value="-write here body-" />
          <ref role="1k5W1q" to="tpen:hshO_Yc" resolve="Comment" />
        </node>
      </node>
      <node concept="3F0ifn" id="5LkYH4D_eBZ" role="3EZMnx">
        <property role="3F0ifm" value="}" />
        <ref role="1k5W1q" to="tpen:hXb$V4T" resolve="RightBracket" />
      </node>
      <node concept="l2Vlx" id="5LkYH4D_ezG" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="5LkYH4D_gxH">
    <ref role="1XX52x" to="zz55:5LkYH4D_eSX" resolve="variable" />
    <node concept="3F0A7n" id="5LkYH4D_gxV" role="2wV5jI">
      <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
    </node>
  </node>
  <node concept="24kQdi" id="5LkYH4D_h0C">
    <ref role="1XX52x" to="zz55:5LkYH4D_eTS" resolve="high" />
    <node concept="3F1sOY" id="5LkYH4D_h0S" role="2wV5jI">
      <ref role="1NtTu8" to="zz55:5LkYH4D_h0L" resolve="value" />
    </node>
  </node>
  <node concept="24kQdi" id="5LkYH4D_h13">
    <ref role="1XX52x" to="zz55:5LkYH4D_eV_" resolve="low" />
    <node concept="3F1sOY" id="5LkYH4D_h18" role="2wV5jI">
      <ref role="1NtTu8" to="zz55:5LkYH4D_h0N" resolve="value" />
    </node>
  </node>
</model>

