<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:118cddf0-50eb-4508-b43c-042dc187977c(renc_mps_iterate.structure)">
  <persistence version="9" />
  <languages>
    <use id="c72da2b9-7cce-4447-8389-f407dc1158b7" name="jetbrains.mps.lang.structure" version="3" />
    <devkit ref="2677cb18-f558-4e33-bc38-a5139cee06dc(jetbrains.mps.devkit.language-design)" />
  </languages>
  <imports>
    <import index="tpee" ref="r:00000000-0000-4000-0000-011c895902ca(jetbrains.mps.baseLanguage.structure)" implicit="true" />
    <import index="tpck" ref="r:00000000-0000-4000-0000-011c89590288(jetbrains.mps.lang.core.structure)" implicit="true" />
  </imports>
  <registry>
    <language id="c72da2b9-7cce-4447-8389-f407dc1158b7" name="jetbrains.mps.lang.structure">
      <concept id="1169125787135" name="jetbrains.mps.lang.structure.structure.AbstractConceptDeclaration" flags="ig" index="PkWjJ">
        <property id="6714410169261853888" name="conceptId" index="EcuMT" />
        <property id="5092175715804935370" name="conceptAlias" index="34LRSv" />
        <child id="1071489727083" name="linkDeclaration" index="1TKVEi" />
      </concept>
      <concept id="1169127622168" name="jetbrains.mps.lang.structure.structure.InterfaceConceptReference" flags="ig" index="PrWs8">
        <reference id="1169127628841" name="intfc" index="PrY4T" />
      </concept>
      <concept id="1071489090640" name="jetbrains.mps.lang.structure.structure.ConceptDeclaration" flags="ig" index="1TIwiD">
        <reference id="1071489389519" name="extends" index="1TJDcQ" />
        <child id="1169129564478" name="implements" index="PzmwI" />
      </concept>
      <concept id="1071489288298" name="jetbrains.mps.lang.structure.structure.LinkDeclaration" flags="ig" index="1TJgyj">
        <property id="1071599776563" name="role" index="20kJfa" />
        <property id="1071599893252" name="sourceCardinality" index="20lbJX" />
        <property id="1071599937831" name="metaClass" index="20lmBu" />
        <property id="241647608299431140" name="linkId" index="IQ2ns" />
        <reference id="1071599976176" name="target" index="20lvS9" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="1TIwiD" id="5LkYH4D_dGf">
    <property role="EcuMT" value="6653218325785795343" />
    <property role="TrG5h" value="Iterate" />
    <property role="34LRSv" value="Iterate loop" />
    <ref role="1TJDcQ" to="tpee:fzclF8l" resolve="Statement" />
    <node concept="1TJgyj" id="5LkYH4D_eTQ" role="1TKVEi">
      <property role="IQ2ns" value="6653218325785800310" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="var" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" node="5LkYH4D_eSX" resolve="variable" />
    </node>
    <node concept="1TJgyj" id="5LkYH4D_eVD" role="1TKVEi">
      <property role="IQ2ns" value="6653218325785800425" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="low" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" node="5LkYH4D_eV_" resolve="low" />
    </node>
    <node concept="1TJgyj" id="5LkYH4D_eVG" role="1TKVEi">
      <property role="IQ2ns" value="6653218325785800428" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="high" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" node="5LkYH4D_eTS" resolve="high" />
    </node>
    <node concept="1TJgyj" id="5LkYH4D_eVK" role="1TKVEi">
      <property role="IQ2ns" value="6653218325785800432" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="body" />
      <property role="20lbJX" value="0..n" />
      <ref role="20lvS9" to="tpee:fzclF8l" resolve="Statement" />
    </node>
  </node>
  <node concept="1TIwiD" id="5LkYH4D_eSX">
    <property role="EcuMT" value="6653218325785800253" />
    <property role="TrG5h" value="variable" />
    <property role="34LRSv" value="variable in iterate loop" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="PrWs8" id="5LkYH4D_eTp" role="PzmwI">
      <ref role="PrY4T" to="tpck:h0TrEE$" resolve="INamedConcept" />
    </node>
  </node>
  <node concept="1TIwiD" id="5LkYH4D_eTS">
    <property role="EcuMT" value="6653218325785800312" />
    <property role="TrG5h" value="high" />
    <property role="34LRSv" value="Max in iterate loop" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="1TJgyj" id="5LkYH4D_h0L" role="1TKVEi">
      <property role="IQ2ns" value="6653218325785808945" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="value" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" to="tpee:fz3vP1J" resolve="Expression" />
    </node>
  </node>
  <node concept="1TIwiD" id="5LkYH4D_eV_">
    <property role="EcuMT" value="6653218325785800421" />
    <property role="TrG5h" value="low" />
    <property role="34LRSv" value="Min in iterate loop" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="1TJgyj" id="5LkYH4D_h0N" role="1TKVEi">
      <property role="IQ2ns" value="6653218325785808947" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="value" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" to="tpee:fz3vP1J" resolve="Expression" />
    </node>
  </node>
</model>

