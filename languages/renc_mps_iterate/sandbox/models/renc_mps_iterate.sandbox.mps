<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:55869d3b-bfb0-45f1-9748-9ec45ed72779(renc_mps_iterate.sandbox)">
  <persistence version="9" />
  <languages>
    <use id="47597633-6d9c-4ab3-8327-da947071bedc" name="renc_mps_iterate" version="-1" />
    <use id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage" version="5" />
  </languages>
  <imports>
    <import index="guwi" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:java.io(JDK/)" />
    <import index="wyt6" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:java.lang(JDK/)" />
  </imports>
  <registry>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1202948039474" name="jetbrains.mps.baseLanguage.structure.InstanceMethodCallOperation" flags="nn" index="liA8E" />
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1070475926800" name="jetbrains.mps.baseLanguage.structure.StringLiteral" flags="nn" index="Xl_RD">
        <property id="1070475926801" name="value" index="Xl_RC" />
      </concept>
      <concept id="1081236700938" name="jetbrains.mps.baseLanguage.structure.StaticMethodDeclaration" flags="ig" index="2YIFZL" />
      <concept id="1070533707846" name="jetbrains.mps.baseLanguage.structure.StaticFieldReference" flags="nn" index="10M0yZ">
        <reference id="1144433057691" name="classifier" index="1PxDUh" />
      </concept>
      <concept id="1070534370425" name="jetbrains.mps.baseLanguage.structure.IntegerType" flags="in" index="10Oyi0" />
      <concept id="1070534760951" name="jetbrains.mps.baseLanguage.structure.ArrayType" flags="in" index="10Q1$e">
        <child id="1070534760952" name="componentType" index="10Q1$1" />
      </concept>
      <concept id="1068390468198" name="jetbrains.mps.baseLanguage.structure.ClassConcept" flags="ig" index="312cEu" />
      <concept id="1068431474542" name="jetbrains.mps.baseLanguage.structure.VariableDeclaration" flags="ng" index="33uBYm">
        <child id="1068431790190" name="initializer" index="33vP2m" />
      </concept>
      <concept id="1092119917967" name="jetbrains.mps.baseLanguage.structure.MulExpression" flags="nn" index="17qRlL" />
      <concept id="1068498886296" name="jetbrains.mps.baseLanguage.structure.VariableReference" flags="nn" index="37vLTw">
        <reference id="1068581517664" name="variableDeclaration" index="3cqZAo" />
      </concept>
      <concept id="1068498886292" name="jetbrains.mps.baseLanguage.structure.ParameterDeclaration" flags="ir" index="37vLTG" />
      <concept id="1225271177708" name="jetbrains.mps.baseLanguage.structure.StringType" flags="in" index="17QB3L" />
      <concept id="4972933694980447171" name="jetbrains.mps.baseLanguage.structure.BaseVariableDeclaration" flags="ng" index="19Szcq">
        <child id="5680397130376446158" name="type" index="1tU5fm" />
      </concept>
      <concept id="1068580123132" name="jetbrains.mps.baseLanguage.structure.BaseMethodDeclaration" flags="ng" index="3clF44">
        <property id="4276006055363816570" name="isSynchronized" index="od$2w" />
        <property id="1181808852946" name="isFinal" index="DiZV1" />
        <child id="1068580123133" name="returnType" index="3clF45" />
        <child id="1068580123134" name="parameter" index="3clF46" />
        <child id="1068580123135" name="body" index="3clF47" />
      </concept>
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123157" name="jetbrains.mps.baseLanguage.structure.Statement" flags="nn" index="3clFbH" />
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1068580320020" name="jetbrains.mps.baseLanguage.structure.IntegerConstant" flags="nn" index="3cmrfG">
        <property id="1068580320021" name="value" index="3cmrfH" />
      </concept>
      <concept id="1068581242875" name="jetbrains.mps.baseLanguage.structure.PlusExpression" flags="nn" index="3cpWs3" />
      <concept id="1068581242864" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclarationStatement" flags="nn" index="3cpWs8">
        <child id="1068581242865" name="localVariableDeclaration" index="3cpWs9" />
      </concept>
      <concept id="1068581242863" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclaration" flags="nr" index="3cpWsn" />
      <concept id="1068581517677" name="jetbrains.mps.baseLanguage.structure.VoidType" flags="in" index="3cqZAl" />
      <concept id="1204053956946" name="jetbrains.mps.baseLanguage.structure.IMethodCall" flags="ng" index="1ndlxa">
        <reference id="1068499141037" name="baseMethodDeclaration" index="37wK5l" />
        <child id="1068499141038" name="actualArgument" index="37wK5m" />
      </concept>
      <concept id="1107461130800" name="jetbrains.mps.baseLanguage.structure.Classifier" flags="ng" index="3pOWGL">
        <child id="5375687026011219971" name="member" index="jymVt" unordered="true" />
      </concept>
      <concept id="7812454656619025416" name="jetbrains.mps.baseLanguage.structure.MethodDeclaration" flags="ng" index="1rXfSm">
        <property id="8355037393041754995" name="isNative" index="2aFKle" />
      </concept>
      <concept id="1081773326031" name="jetbrains.mps.baseLanguage.structure.BinaryOperation" flags="nn" index="3uHJSO">
        <child id="1081773367579" name="rightExpression" index="3uHU7w" />
        <child id="1081773367580" name="leftExpression" index="3uHU7B" />
      </concept>
      <concept id="1178549954367" name="jetbrains.mps.baseLanguage.structure.IVisible" flags="ng" index="1B3ioH">
        <child id="1178549979242" name="visibility" index="1B3o_S" />
      </concept>
      <concept id="1146644602865" name="jetbrains.mps.baseLanguage.structure.PublicVisibility" flags="nn" index="3Tm1VV" />
    </language>
    <language id="47597633-6d9c-4ab3-8327-da947071bedc" name="renc_mps_iterate">
      <concept id="6653218325785800421" name="renc_mps_iterate.structure.low" flags="ng" index="1PK1gO">
        <child id="6653218325785808947" name="value" index="1PKuFy" />
      </concept>
      <concept id="6653218325785800312" name="renc_mps_iterate.structure.high" flags="ng" index="1PK1iD">
        <child id="6653218325785808945" name="value" index="1PKuFw" />
      </concept>
      <concept id="6653218325785800253" name="renc_mps_iterate.structure.variable" flags="ng" index="1PK1jG" />
      <concept id="6653218325785795343" name="renc_mps_iterate.structure.Iterate" flags="ng" index="1PK27u">
        <child id="6653218325785800432" name="body" index="1PK1gx" />
        <child id="6653218325785800425" name="low" index="1PK1gS" />
        <child id="6653218325785800428" name="high" index="1PK1gX" />
        <child id="6653218325785800310" name="var" index="1PK1iB" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="312cEu" id="5LkYH4D_dhn">
    <property role="TrG5h" value="Sample" />
    <node concept="2YIFZL" id="5LkYH4D_diE" role="jymVt">
      <property role="TrG5h" value="main" />
      <property role="od$2w" value="false" />
      <property role="DiZV1" value="false" />
      <property role="2aFKle" value="false" />
      <node concept="3clFbS" id="5LkYH4D_diH" role="3clF47">
        <node concept="3clFbH" id="5LkYH4D_joY" role="3cqZAp" />
        <node concept="3cpWs8" id="5LkYH4D_E03" role="3cqZAp">
          <node concept="3cpWsn" id="5LkYH4D_E06" role="3cpWs9">
            <property role="TrG5h" value="a" />
            <node concept="10Oyi0" id="5LkYH4D_E01" role="1tU5fm" />
            <node concept="3cmrfG" id="5LkYH4D_E3r" role="33vP2m">
              <property role="3cmrfH" value="3" />
            </node>
          </node>
        </node>
        <node concept="1PK27u" id="5LkYH4DAYW9" role="3cqZAp">
          <node concept="3clFbF" id="5LkYH4DAZ2m" role="1PK1gx">
            <node concept="2OqwBi" id="5LkYH4DAZ2j" role="3clFbG">
              <node concept="10M0yZ" id="5LkYH4DAZ2k" role="2Oq$k0">
                <ref role="1PxDUh" to="wyt6:~System" resolve="System" />
                <ref role="3cqZAo" to="wyt6:~System.out" resolve="out" />
              </node>
              <node concept="liA8E" id="5LkYH4DAZ2l" role="2OqNvi">
                <ref role="37wK5l" to="guwi:~PrintStream.println(java.lang.String):void" resolve="println" />
                <node concept="Xl_RD" id="5LkYH4DAZ3v" role="37wK5m">
                  <property role="Xl_RC" value="iteruju" />
                </node>
              </node>
            </node>
          </node>
          <node concept="1PK1jG" id="5LkYH4DAYWb" role="1PK1iB">
            <property role="TrG5h" value="i" />
          </node>
          <node concept="1PK1gO" id="5LkYH4DAYWd" role="1PK1gS">
            <node concept="3cpWs3" id="5LkYH4DCjQN" role="1PKuFy">
              <node concept="17qRlL" id="5LkYH4DCiKK" role="3uHU7B">
                <node concept="3cmrfG" id="5LkYH4DCieN" role="3uHU7B">
                  <property role="3cmrfH" value="2" />
                </node>
                <node concept="3cmrfG" id="5LkYH4DCjwq" role="3uHU7w">
                  <property role="3cmrfH" value="2" />
                </node>
              </node>
              <node concept="3cmrfG" id="5LkYH4DClOQ" role="3uHU7w">
                <property role="3cmrfH" value="1" />
              </node>
            </node>
          </node>
          <node concept="1PK1iD" id="5LkYH4DAYWh" role="1PK1gX">
            <node concept="3cpWs3" id="5LkYH4DCdln" role="1PKuFw">
              <node concept="37vLTw" id="5LkYH4DCdm6" role="3uHU7w">
                <ref role="3cqZAo" node="5LkYH4D_E06" resolve="a" />
              </node>
              <node concept="3cmrfG" id="5LkYH4DAZ1_" role="3uHU7B">
                <property role="3cmrfH" value="5" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="5LkYH4DAYZU" role="3cqZAp" />
        <node concept="3clFbH" id="5LkYH4DAYXY" role="3cqZAp" />
        <node concept="3clFbH" id="5LkYH4D_hVA" role="3cqZAp" />
      </node>
      <node concept="3Tm1VV" id="5LkYH4D_dij" role="1B3o_S" />
      <node concept="3cqZAl" id="5LkYH4D_diz" role="3clF45" />
      <node concept="37vLTG" id="5LkYH4D_dj1" role="3clF46">
        <property role="TrG5h" value="args" />
        <node concept="10Q1$e" id="5LkYH4D_dDh" role="1tU5fm">
          <node concept="17QB3L" id="5LkYH4D_dj0" role="10Q1$1" />
        </node>
      </node>
    </node>
    <node concept="3Tm1VV" id="5LkYH4D_dho" role="1B3o_S" />
  </node>
</model>

