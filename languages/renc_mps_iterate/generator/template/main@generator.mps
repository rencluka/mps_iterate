<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:60354995-6d73-41b6-9150-10833e5c6da7(main@generator)">
  <persistence version="9" />
  <languages>
    <devkit ref="a2eb3a43-fcc2-4200-80dc-c60110c4862d(jetbrains.mps.devkit.templates)" />
  </languages>
  <imports>
    <import index="zz55" ref="r:118cddf0-50eb-4508-b43c-042dc187977c(renc_mps_iterate.structure)" />
    <import index="tpee" ref="r:00000000-0000-4000-0000-011c895902ca(jetbrains.mps.baseLanguage.structure)" implicit="true" />
    <import index="wyt6" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:java.lang(JDK/)" implicit="true" />
    <import index="guwi" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:java.io(JDK/)" implicit="true" />
  </imports>
  <registry>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1202948039474" name="jetbrains.mps.baseLanguage.structure.InstanceMethodCallOperation" flags="nn" index="liA8E" />
      <concept id="1239714755177" name="jetbrains.mps.baseLanguage.structure.AbstractUnaryNumberOperation" flags="nn" index="2$Kvd9">
        <child id="1239714902950" name="expression" index="2$L3a6" />
      </concept>
      <concept id="1154032098014" name="jetbrains.mps.baseLanguage.structure.AbstractLoopStatement" flags="nn" index="2LF5Ji">
        <child id="1154032183016" name="body" index="2LFqv$" />
      </concept>
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1137021947720" name="jetbrains.mps.baseLanguage.structure.ConceptFunction" flags="in" index="2VMwT0">
        <child id="1137022507850" name="body" index="2VODD2" />
      </concept>
      <concept id="1070475926800" name="jetbrains.mps.baseLanguage.structure.StringLiteral" flags="nn" index="Xl_RD">
        <property id="1070475926801" name="value" index="Xl_RC" />
      </concept>
      <concept id="1081236700938" name="jetbrains.mps.baseLanguage.structure.StaticMethodDeclaration" flags="ig" index="2YIFZL" />
      <concept id="1070533707846" name="jetbrains.mps.baseLanguage.structure.StaticFieldReference" flags="nn" index="10M0yZ">
        <reference id="1144433057691" name="classifier" index="1PxDUh" />
      </concept>
      <concept id="1070534370425" name="jetbrains.mps.baseLanguage.structure.IntegerType" flags="in" index="10Oyi0" />
      <concept id="1070534760951" name="jetbrains.mps.baseLanguage.structure.ArrayType" flags="in" index="10Q1$e">
        <child id="1070534760952" name="componentType" index="10Q1$1" />
      </concept>
      <concept id="1068390468198" name="jetbrains.mps.baseLanguage.structure.ClassConcept" flags="ig" index="312cEu" />
      <concept id="1068431474542" name="jetbrains.mps.baseLanguage.structure.VariableDeclaration" flags="ng" index="33uBYm">
        <child id="1068431790190" name="initializer" index="33vP2m" />
      </concept>
      <concept id="1068498886296" name="jetbrains.mps.baseLanguage.structure.VariableReference" flags="nn" index="37vLTw">
        <reference id="1068581517664" name="variableDeclaration" index="3cqZAo" />
      </concept>
      <concept id="1068498886292" name="jetbrains.mps.baseLanguage.structure.ParameterDeclaration" flags="ir" index="37vLTG" />
      <concept id="1225271177708" name="jetbrains.mps.baseLanguage.structure.StringType" flags="in" index="17QB3L" />
      <concept id="4972933694980447171" name="jetbrains.mps.baseLanguage.structure.BaseVariableDeclaration" flags="ng" index="19Szcq">
        <child id="5680397130376446158" name="type" index="1tU5fm" />
      </concept>
      <concept id="1068580123132" name="jetbrains.mps.baseLanguage.structure.BaseMethodDeclaration" flags="ng" index="3clF44">
        <child id="1068580123133" name="returnType" index="3clF45" />
        <child id="1068580123134" name="parameter" index="3clF46" />
        <child id="1068580123135" name="body" index="3clF47" />
      </concept>
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123157" name="jetbrains.mps.baseLanguage.structure.Statement" flags="nn" index="3clFbH" />
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1068580320020" name="jetbrains.mps.baseLanguage.structure.IntegerConstant" flags="nn" index="3cmrfG">
        <property id="1068580320021" name="value" index="3cmrfH" />
      </concept>
      <concept id="1068581242864" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclarationStatement" flags="nn" index="3cpWs8">
        <child id="1068581242865" name="localVariableDeclaration" index="3cpWs9" />
      </concept>
      <concept id="1068581242863" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclaration" flags="nr" index="3cpWsn" />
      <concept id="1068581517677" name="jetbrains.mps.baseLanguage.structure.VoidType" flags="in" index="3cqZAl" />
      <concept id="1081506773034" name="jetbrains.mps.baseLanguage.structure.LessThanExpression" flags="nn" index="3eOVzh" />
      <concept id="1204053956946" name="jetbrains.mps.baseLanguage.structure.IMethodCall" flags="ng" index="1ndlxa">
        <reference id="1068499141037" name="baseMethodDeclaration" index="37wK5l" />
        <child id="1068499141038" name="actualArgument" index="37wK5m" />
      </concept>
      <concept id="1107461130800" name="jetbrains.mps.baseLanguage.structure.Classifier" flags="ng" index="3pOWGL">
        <child id="5375687026011219971" name="member" index="jymVt" unordered="true" />
      </concept>
      <concept id="1081773326031" name="jetbrains.mps.baseLanguage.structure.BinaryOperation" flags="nn" index="3uHJSO">
        <child id="1081773367579" name="rightExpression" index="3uHU7w" />
        <child id="1081773367580" name="leftExpression" index="3uHU7B" />
      </concept>
      <concept id="1214918800624" name="jetbrains.mps.baseLanguage.structure.PostfixIncrementExpression" flags="nn" index="3uNrnE" />
      <concept id="1178549954367" name="jetbrains.mps.baseLanguage.structure.IVisible" flags="ng" index="1B3ioH">
        <child id="1178549979242" name="visibility" index="1B3o_S" />
      </concept>
      <concept id="1144230876926" name="jetbrains.mps.baseLanguage.structure.AbstractForStatement" flags="nn" index="1DupvO">
        <child id="1144230900587" name="variable" index="1Duv9x" />
      </concept>
      <concept id="1144231330558" name="jetbrains.mps.baseLanguage.structure.ForStatement" flags="nn" index="1Dw8fO">
        <child id="1144231399730" name="condition" index="1Dwp0S" />
        <child id="1144231408325" name="iteration" index="1Dwrff" />
      </concept>
      <concept id="1146644602865" name="jetbrains.mps.baseLanguage.structure.PublicVisibility" flags="nn" index="3Tm1VV" />
    </language>
    <language id="b401a680-8325-4110-8fd3-84331ff25bef" name="jetbrains.mps.lang.generator">
      <concept id="1114706874351" name="jetbrains.mps.lang.generator.structure.CopySrcNodeMacro" flags="ln" index="29HgVG">
        <child id="1168024447342" name="sourceNodeQuery" index="3NFExx" />
      </concept>
      <concept id="1114729360583" name="jetbrains.mps.lang.generator.structure.CopySrcListMacro" flags="ln" index="2b32R4">
        <child id="1168278589236" name="sourceNodesQuery" index="2P8S$" />
      </concept>
      <concept id="1095416546421" name="jetbrains.mps.lang.generator.structure.MappingConfiguration" flags="ig" index="bUwia">
        <child id="1200911492601" name="mappingLabel" index="2rTMjI" />
        <child id="1167328349397" name="reductionMappingRule" index="3acgRq" />
      </concept>
      <concept id="1168559333462" name="jetbrains.mps.lang.generator.structure.TemplateDeclarationReference" flags="ln" index="j$656" />
      <concept id="1095672379244" name="jetbrains.mps.lang.generator.structure.TemplateFragment" flags="ng" index="raruj" />
      <concept id="1200911316486" name="jetbrains.mps.lang.generator.structure.MappingLabelDeclaration" flags="lg" index="2rT7sh">
        <reference id="1200911342686" name="sourceConcept" index="2rTdP9" />
        <reference id="1200913004646" name="targetConcept" index="2rZz_L" />
      </concept>
      <concept id="1722980698497626400" name="jetbrains.mps.lang.generator.structure.ITemplateCall" flags="ng" index="v9R3L">
        <reference id="1722980698497626483" name="template" index="v9R2y" />
      </concept>
      <concept id="1167168920554" name="jetbrains.mps.lang.generator.structure.BaseMappingRule_Condition" flags="in" index="30G5F_" />
      <concept id="1167169188348" name="jetbrains.mps.lang.generator.structure.TemplateFunctionParameter_sourceNode" flags="nn" index="30H73N" />
      <concept id="1167169308231" name="jetbrains.mps.lang.generator.structure.BaseMappingRule" flags="ng" index="30H$t8">
        <reference id="1167169349424" name="applicableConcept" index="30HIoZ" />
        <child id="1167169362365" name="conditionFunction" index="30HLyM" />
      </concept>
      <concept id="1092059087312" name="jetbrains.mps.lang.generator.structure.TemplateDeclaration" flags="ig" index="13MO4I">
        <reference id="1168285871518" name="applicableConcept" index="3gUMe" />
        <child id="1092060348987" name="contentNode" index="13RCb5" />
      </concept>
      <concept id="1087833241328" name="jetbrains.mps.lang.generator.structure.PropertyMacro" flags="ln" index="17Uvod">
        <child id="1167756362303" name="propertyValueFunction" index="3zH0cK" />
      </concept>
      <concept id="1167327847730" name="jetbrains.mps.lang.generator.structure.Reduction_MappingRule" flags="lg" index="3aamgX">
        <child id="1169672767469" name="ruleConsequence" index="1lVwrX" />
      </concept>
      <concept id="1167756080639" name="jetbrains.mps.lang.generator.structure.PropertyMacro_GetPropertyValue" flags="in" index="3zFVjK" />
      <concept id="1167770111131" name="jetbrains.mps.lang.generator.structure.ReferenceMacro_GetReferent" flags="in" index="3$xsQk" />
      <concept id="1167951910403" name="jetbrains.mps.lang.generator.structure.SourceSubstituteMacro_SourceNodesQuery" flags="in" index="3JmXsc" />
      <concept id="1168024337012" name="jetbrains.mps.lang.generator.structure.SourceSubstituteMacro_SourceNodeQuery" flags="in" index="3NFfHV" />
      <concept id="1088761943574" name="jetbrains.mps.lang.generator.structure.ReferenceMacro" flags="ln" index="1ZhdrF">
        <child id="1167770376702" name="referentFunction" index="3$ytzL" />
      </concept>
    </language>
    <language id="d7706f63-9be2-479c-a3da-ae92af1e64d5" name="jetbrains.mps.lang.generator.generationContext">
      <concept id="1218047638031" name="jetbrains.mps.lang.generator.generationContext.structure.GenerationContextOp_CreateUniqueName" flags="nn" index="2piZGk">
        <child id="1218047638032" name="baseName" index="2piZGb" />
        <child id="1218049772449" name="contextNode" index="2pr8EU" />
      </concept>
      <concept id="1216860049627" name="jetbrains.mps.lang.generator.generationContext.structure.GenerationContextOp_GetOutputByLabelAndInput" flags="nn" index="1iwH70">
        <reference id="1216860049628" name="label" index="1iwH77" />
        <child id="1216860049632" name="inputNode" index="1iwH7V" />
      </concept>
      <concept id="1216860049635" name="jetbrains.mps.lang.generator.generationContext.structure.TemplateFunctionParameter_generationContext" flags="nn" index="1iwH7S" />
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="1172008320231" name="jetbrains.mps.lang.smodel.structure.Node_IsNotNullOperation" flags="nn" index="3x8VRR" />
      <concept id="1138056143562" name="jetbrains.mps.lang.smodel.structure.SLinkAccess" flags="nn" index="3TrEf2">
        <reference id="1138056516764" name="link" index="3Tt5mk" />
      </concept>
      <concept id="1138056282393" name="jetbrains.mps.lang.smodel.structure.SLinkListAccess" flags="nn" index="3Tsc0h">
        <reference id="1138056546658" name="link" index="3TtcxE" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <child id="5169995583184591170" name="smodelAttribute" index="lGtFl" />
      </concept>
      <concept id="3364660638048049750" name="jetbrains.mps.lang.core.structure.PropertyAttribute" flags="ng" index="A9Btg">
        <property id="1757699476691236117" name="propertyName" index="2qtEX9" />
        <property id="1341860900487648621" name="propertyId" index="P4ACc" />
      </concept>
      <concept id="3364660638048049745" name="jetbrains.mps.lang.core.structure.LinkAttribute" flags="ng" index="A9Btn">
        <property id="1757699476691236116" name="linkRole" index="2qtEX8" />
        <property id="1341860900488019036" name="linkId" index="P3scX" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="bUwia" id="5LkYH4D_cSI">
    <property role="TrG5h" value="main" />
    <node concept="2rT7sh" id="5LkYH4DABWN" role="2rTMjI">
      <property role="TrG5h" value="varDecl" />
      <ref role="2rTdP9" to="zz55:5LkYH4D_eSX" resolve="variable" />
      <ref role="2rZz_L" to="tpee:fzcpWvJ" resolve="LocalVariableDeclaration" />
    </node>
    <node concept="3aamgX" id="5LkYH4D_nFe" role="3acgRq">
      <ref role="30HIoZ" to="zz55:5LkYH4D_dGf" resolve="Iterate" />
      <node concept="j$656" id="5LkYH4D_nFk" role="1lVwrX">
        <ref role="v9R2y" node="5LkYH4D_nFi" resolve="reduce_Iterate" />
      </node>
      <node concept="30G5F_" id="5LkYH4D_I$Q" role="30HLyM">
        <node concept="3clFbS" id="5LkYH4D_I$R" role="2VODD2">
          <node concept="3clFbF" id="5LkYH4D_Jzt" role="3cqZAp">
            <node concept="2OqwBi" id="5LkYH4D_JMK" role="3clFbG">
              <node concept="30H73N" id="5LkYH4D_Jzs" role="2Oq$k0" />
              <node concept="3x8VRR" id="5LkYH4D_KfG" role="2OqNvi" />
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="3aamgX" id="5LkYH4D__79" role="3acgRq">
      <ref role="30HIoZ" to="zz55:5LkYH4D_eTS" resolve="high" />
      <node concept="j$656" id="5LkYH4D__7h" role="1lVwrX">
        <ref role="v9R2y" node="5LkYH4D__7f" resolve="reduce_high" />
      </node>
      <node concept="30G5F_" id="5LkYH4D_KsP" role="30HLyM">
        <node concept="3clFbS" id="5LkYH4D_KsQ" role="2VODD2">
          <node concept="3clFbF" id="5LkYH4D_KzZ" role="3cqZAp">
            <node concept="2OqwBi" id="5LkYH4D_KKa" role="3clFbG">
              <node concept="30H73N" id="5LkYH4D_KzY" role="2Oq$k0" />
              <node concept="3x8VRR" id="5LkYH4D_L2Y" role="2OqNvi" />
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="3aamgX" id="5LkYH4D_Cm6" role="3acgRq">
      <ref role="30HIoZ" to="zz55:5LkYH4D_eV_" resolve="low" />
      <node concept="j$656" id="5LkYH4D_Cmg" role="1lVwrX">
        <ref role="v9R2y" node="5LkYH4D_Cme" resolve="reduce_low" />
      </node>
      <node concept="30G5F_" id="5LkYH4D_Lfz" role="30HLyM">
        <node concept="3clFbS" id="5LkYH4D_Lf$" role="2VODD2">
          <node concept="3clFbF" id="5LkYH4D_LmH" role="3cqZAp">
            <node concept="2OqwBi" id="5LkYH4D_LyS" role="3clFbG">
              <node concept="30H73N" id="5LkYH4D_LmG" role="2Oq$k0" />
              <node concept="3x8VRR" id="5LkYH4D_LPG" role="2OqNvi" />
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="13MO4I" id="5LkYH4D_nFi">
    <property role="TrG5h" value="reduce_Iterate" />
    <ref role="3gUMe" to="zz55:5LkYH4D_dGf" resolve="Iterate" />
    <node concept="312cEu" id="5LkYH4D_nFn" role="13RCb5">
      <property role="TrG5h" value="sampleabcd" />
      <node concept="2YIFZL" id="5LkYH4D_nNS" role="jymVt">
        <property role="TrG5h" value="main" />
        <node concept="37vLTG" id="5LkYH4D_nNT" role="3clF46">
          <property role="TrG5h" value="args" />
          <node concept="10Q1$e" id="5LkYH4D_nNU" role="1tU5fm">
            <node concept="17QB3L" id="5LkYH4D_nNV" role="10Q1$1" />
          </node>
        </node>
        <node concept="3cqZAl" id="5LkYH4D_nNW" role="3clF45" />
        <node concept="3Tm1VV" id="5LkYH4D_nNX" role="1B3o_S" />
        <node concept="3clFbS" id="5LkYH4D_nNY" role="3clF47">
          <node concept="3cpWs8" id="5LkYH4D_nPS" role="3cqZAp">
            <node concept="3cpWsn" id="5LkYH4D_nPV" role="3cpWs9">
              <property role="TrG5h" value="a" />
              <node concept="10Oyi0" id="5LkYH4D_nPR" role="1tU5fm" />
              <node concept="3cmrfG" id="5LkYH4D_nQL" role="33vP2m">
                <property role="3cmrfH" value="4" />
              </node>
            </node>
          </node>
          <node concept="3clFbH" id="5LkYH4D_rol" role="3cqZAp" />
          <node concept="1Dw8fO" id="5LkYH4D_rpU" role="3cqZAp">
            <node concept="3clFbS" id="5LkYH4D_rpW" role="2LFqv$">
              <node concept="3clFbF" id="5LkYH4D_uup" role="3cqZAp">
                <node concept="2OqwBi" id="5LkYH4D_uum" role="3clFbG">
                  <node concept="10M0yZ" id="5LkYH4D_uun" role="2Oq$k0">
                    <ref role="1PxDUh" to="wyt6:~System" resolve="System" />
                    <ref role="3cqZAo" to="wyt6:~System.out" resolve="out" />
                  </node>
                  <node concept="liA8E" id="5LkYH4D_uuo" role="2OqNvi">
                    <ref role="37wK5l" to="guwi:~PrintStream.println(java.lang.String):void" resolve="println" />
                    <node concept="Xl_RD" id="5LkYH4D_uvi" role="37wK5m">
                      <property role="Xl_RC" value="iterating" />
                    </node>
                    <node concept="1ZhdrF" id="5LkYH4DC855" role="lGtFl">
                      <property role="P3scX" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1204053956946/1068499141037" />
                      <property role="2qtEX8" value="baseMethodDeclaration" />
                      <node concept="3$xsQk" id="5LkYH4DC856" role="3$ytzL">
                        <node concept="3clFbS" id="5LkYH4DC857" role="2VODD2">
                          <node concept="3clFbF" id="5LkYH4DC8j7" role="3cqZAp">
                            <node concept="2OqwBi" id="5LkYH4DC8xV" role="3clFbG">
                              <node concept="1iwH7S" id="5LkYH4DC8j6" role="2Oq$k0" />
                              <node concept="1iwH70" id="5LkYH4DC8EM" role="2OqNvi">
                                <ref role="1iwH77" node="5LkYH4DABWN" resolve="varDecl" />
                                <node concept="30H73N" id="5LkYH4DC8W2" role="1iwH7V" />
                              </node>
                            </node>
                          </node>
                          <node concept="3clFbH" id="5LkYH4DC9wH" role="3cqZAp" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="2b32R4" id="5LkYH4D_NXs" role="lGtFl">
                  <node concept="3JmXsc" id="5LkYH4D_NXv" role="2P8S$">
                    <node concept="3clFbS" id="5LkYH4D_NXw" role="2VODD2">
                      <node concept="3clFbF" id="5LkYH4D_NXA" role="3cqZAp">
                        <node concept="2OqwBi" id="5LkYH4D_NXx" role="3clFbG">
                          <node concept="3Tsc0h" id="5LkYH4D_NX$" role="2OqNvi">
                            <ref role="3TtcxE" to="zz55:5LkYH4D_eVK" resolve="body" />
                          </node>
                          <node concept="30H73N" id="5LkYH4D_NX_" role="2Oq$k0" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3clFbH" id="5LkYH4D_rpV" role="3cqZAp" />
            </node>
            <node concept="3cpWsn" id="5LkYH4D_rpX" role="1Duv9x">
              <property role="TrG5h" value="i" />
              <node concept="10Oyi0" id="5LkYH4D_rqn" role="1tU5fm" />
              <node concept="3cmrfG" id="5LkYH4D_rrG" role="33vP2m">
                <property role="3cmrfH" value="4" />
                <node concept="29HgVG" id="5LkYH4D_zWx" role="lGtFl">
                  <node concept="3NFfHV" id="5LkYH4D_zWy" role="3NFExx">
                    <node concept="3clFbS" id="5LkYH4D_zWz" role="2VODD2">
                      <node concept="3clFbF" id="5LkYH4D_zWD" role="3cqZAp">
                        <node concept="2OqwBi" id="5LkYH4D_zW$" role="3clFbG">
                          <node concept="3TrEf2" id="5LkYH4D_zWB" role="2OqNvi">
                            <ref role="3Tt5mk" to="zz55:5LkYH4D_eVD" resolve="low" />
                          </node>
                          <node concept="30H73N" id="5LkYH4D_zWC" role="2Oq$k0" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
              <node concept="17Uvod" id="5LkYH4DCn1k" role="lGtFl">
                <property role="P4ACc" value="ceab5195-25ea-4f22-9b92-103b95ca8c0c/1169194658468/1169194664001" />
                <property role="2qtEX9" value="name" />
                <node concept="3zFVjK" id="5LkYH4DCn1l" role="3zH0cK">
                  <node concept="3clFbS" id="5LkYH4DCn1m" role="2VODD2">
                    <node concept="3clFbF" id="5LkYH4DCnmb" role="3cqZAp">
                      <node concept="2OqwBi" id="5LkYH4DCo2W" role="3clFbG">
                        <node concept="1iwH7S" id="5LkYH4DCnma" role="2Oq$k0" />
                        <node concept="2piZGk" id="5LkYH4DCojt" role="2OqNvi">
                          <node concept="Xl_RD" id="5LkYH4DCpn$" role="2piZGb">
                            <property role="Xl_RC" value="myVariable" />
                          </node>
                          <node concept="30H73N" id="5LkYH4DCo$N" role="2pr8EU" />
                        </node>
                      </node>
                    </node>
                    <node concept="3clFbH" id="5LkYH4DCwCf" role="3cqZAp" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3eOVzh" id="5LkYH4D_tzq" role="1Dwp0S">
              <node concept="3cmrfG" id="5LkYH4D_t$4" role="3uHU7w">
                <property role="3cmrfH" value="16" />
                <node concept="29HgVG" id="5LkYH4D_$8z" role="lGtFl">
                  <node concept="3NFfHV" id="5LkYH4D_$8$" role="3NFExx">
                    <node concept="3clFbS" id="5LkYH4D_$8_" role="2VODD2">
                      <node concept="3clFbF" id="5LkYH4D_$8F" role="3cqZAp">
                        <node concept="2OqwBi" id="5LkYH4D_$8A" role="3clFbG">
                          <node concept="3TrEf2" id="5LkYH4D_$8D" role="2OqNvi">
                            <ref role="3Tt5mk" to="zz55:5LkYH4D_eVG" resolve="high" />
                          </node>
                          <node concept="30H73N" id="5LkYH4D_$8E" role="2Oq$k0" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
              <node concept="37vLTw" id="5LkYH4D_s6S" role="3uHU7B">
                <ref role="3cqZAo" node="5LkYH4D_rpX" resolve="i" />
              </node>
            </node>
            <node concept="3uNrnE" id="5LkYH4D_uov" role="1Dwrff">
              <node concept="37vLTw" id="5LkYH4D_uox" role="2$L3a6">
                <ref role="3cqZAo" node="5LkYH4D_rpX" resolve="i" />
              </node>
            </node>
            <node concept="raruj" id="5LkYH4D_u$U" role="lGtFl" />
          </node>
          <node concept="3clFbH" id="5LkYH4D_nR5" role="3cqZAp" />
        </node>
      </node>
      <node concept="3Tm1VV" id="5LkYH4D_nFo" role="1B3o_S" />
    </node>
  </node>
  <node concept="13MO4I" id="5LkYH4D__7f">
    <property role="TrG5h" value="reduce_high" />
    <ref role="3gUMe" to="zz55:5LkYH4D_eTS" resolve="high" />
    <node concept="3cmrfG" id="5LkYH4D__7k" role="13RCb5">
      <property role="3cmrfH" value="4" />
      <node concept="raruj" id="5LkYH4D_AEd" role="lGtFl" />
      <node concept="29HgVG" id="5LkYH4D_BoX" role="lGtFl">
        <node concept="3NFfHV" id="5LkYH4D_BoY" role="3NFExx">
          <node concept="3clFbS" id="5LkYH4D_BoZ" role="2VODD2">
            <node concept="3clFbF" id="5LkYH4D_Bp5" role="3cqZAp">
              <node concept="2OqwBi" id="5LkYH4D_Bp0" role="3clFbG">
                <node concept="3TrEf2" id="5LkYH4D_Bp3" role="2OqNvi">
                  <ref role="3Tt5mk" to="zz55:5LkYH4D_h0L" resolve="value" />
                </node>
                <node concept="30H73N" id="5LkYH4D_Bp4" role="2Oq$k0" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="13MO4I" id="5LkYH4D_Cme">
    <property role="TrG5h" value="reduce_low" />
    <ref role="3gUMe" to="zz55:5LkYH4D_eV_" resolve="low" />
    <node concept="3cmrfG" id="5LkYH4D_Cmj" role="13RCb5">
      <property role="3cmrfH" value="2" />
      <node concept="raruj" id="5LkYH4D_Cmr" role="lGtFl" />
      <node concept="29HgVG" id="5LkYH4D_Cmx" role="lGtFl">
        <node concept="3NFfHV" id="5LkYH4D_Cmy" role="3NFExx">
          <node concept="3clFbS" id="5LkYH4D_Cmz" role="2VODD2">
            <node concept="3clFbF" id="5LkYH4D_CmD" role="3cqZAp">
              <node concept="2OqwBi" id="5LkYH4D_Cm$" role="3clFbG">
                <node concept="3TrEf2" id="5LkYH4D_CmB" role="2OqNvi">
                  <ref role="3Tt5mk" to="zz55:5LkYH4D_h0N" resolve="value" />
                </node>
                <node concept="30H73N" id="5LkYH4D_CmC" role="2Oq$k0" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
</model>

